package cn.kgc.tangcco.utils.servlet;

import com.alibaba.fastjson.JSON;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/25 9:40
 */
public abstract class BaseJsonp {
    public static String getResponseText(String callback ,String json) {
        return callback + "(" + json + ")";
    }
    public static String getResponseText(String callback ,Object object) {
        return getResponseText(callback, JSON.toJSONString(object));
    }
}
