package cn.kgc.tangcco.utils.servlet;

import com.alibaba.fastjson.JSON;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/25 9:30
 */
public class BaseServlet extends HttpServlet {
    private static final long serialVersionUID = -2328173474798552994L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        excute(request,response);
    }

    @Override
    protected long getLastModified(HttpServletRequest request) {
        return super.getLastModified(request);
    }

    @Override
    protected void doHead(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        excute(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        excute(request,response);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        excute(request,response);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        excute(request,response);
    }

    @Override
    protected void doOptions(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        excute(request,response);
    }

    @Override
    protected void doTrace(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        excute(request,response);
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        excute(request,response);
    }

    /**
     * 执行真实方法
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     */
    public void excute(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        // 处理中文乱码
        // 处理post请求的中文乱码
        // request.setCharacterEncoding("utf-8");
        // 处理响应的字符集中文乱码
        // response.setCharacterEncoding("utf-8");
        // 处理浏览器字符集中文乱码
        // response.setContentType("text/html;charset=utf-8");
        System.out.println("请求方式:" + request.getMethod());
        String methodName = request.getParameter("methodName");
        Class<? extends BaseServlet> clazz = this.getClass();
        String contentType = request.getHeader("content-type");
        if (!StringUtils.isBlank(contentType)){
            if (contentType.contains("application/json")){
                try {
                    Method method = clazz.getDeclaredMethod(methodName, HttpServletRequest.class, HttpServletResponse.class,String.class);
                    method.setAccessible(true);

                    String text = IOUtils.toString(request.getInputStream(), "utf-8");
                    method.invoke(this,request,response,text);
                } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | IOException e) {
                    e.printStackTrace();
                }
            }else{
                //
                try {
                    Method method = clazz.getDeclaredMethod(methodName, HttpServletRequest.class, HttpServletResponse.class);
                    method.setAccessible(true);
                    method.invoke(this,request,response);
                } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }else{
            try {
                Method method = clazz.getDeclaredMethod(methodName, HttpServletRequest.class, HttpServletResponse.class);
                method.setAccessible(true);
                method.invoke(this,request,response);
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }


    }

    /**
     * 向页面响应字符串
     * @param response  HttpServletResponse
     * @param Text  向页面响应的字符串
     * @throws IOException  IOException
     */
    public static void printText(HttpServletResponse response ,String Text) throws IOException {
        PrintWriter writer = response.getWriter();
        writer.print(Text);
        writer.flush();
        writer.close();
    }
    /**
     * 向页面响应json字符串
     * @param response  HttpServletResponse
     * @param object  向页面响应的对象
     * @throws IOException  IOException
     */
    public static void printJsonObject(HttpServletResponse response ,Object object) throws IOException {
        PrintWriter writer = response.getWriter();
        writer.print(JSON.toJSONString(object));
        writer.flush();
        writer.close();
    }

    /**
     *
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param path 请求
     * @throws ServletException
     * @throws IOException
     */
    public static void forword(HttpServletRequest request,HttpServletResponse response,String path) throws ServletException, IOException {
        request.getRequestDispatcher(path).forward(request,response);
    }
    public static void forwordToWEB_INF(HttpServletRequest request,HttpServletResponse response,String path) throws ServletException, IOException {
        forword(request,response,"/WEB-INF/" + path);
    }
    public static void forwordSuffixJSP(HttpServletRequest request,HttpServletResponse response,String path) throws ServletException, IOException {
        forword(request,response,path + ".jsp");
    }
    public static void forwordToWEB_INF_SuffixJSP(HttpServletRequest request,HttpServletResponse response,String path) throws ServletException, IOException {
        forword(request,response,"/WEB-INF/" + path  + ".jsp");
    }

    public static void redirect(HttpServletRequest request,HttpServletResponse response,String path) throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + "/"+ path);
    }
    public static void redirectSuffixJSP(HttpServletRequest request,HttpServletResponse response,String path) throws ServletException, IOException {
        redirect(request,response,path + ".jsp");
    }
}
