package cn.kgc.tangcco.model;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/13 13:47
 */
public enum Gender {
    MALE(1), FEMALE(0);
    /**
     * 1代表男性 0代表女性
     */
    private Integer sex;

    Gender(Integer sex) {
        this.sex = sex;
    }

    public Integer getSex() {
        return sex;
    }

}
