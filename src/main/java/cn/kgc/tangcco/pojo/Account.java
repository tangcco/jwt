package cn.kgc.tangcco.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author 李昊哲
 * @Description 用户账号 用于登录和注册
 * @create 2021/1/23 11:12
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Account implements Serializable {
    private static final long serialVersionUID = 3178898191879023281L;
    /**
     * 账号主键ID
     */
    private Long aid;
    /**
     * 账号为手机号
     */
    private String mobile;
    /**
     * 密码
     */
    private String authText;
    /**
     * 账号创建时间
     */
    @JSONField(format = "yyyyMMdd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 使用手机号和密码登录的时候构建对象
     *
     * @param mobile   手机号
     * @param authText 密码
     */
    public Account(String mobile, String authText) {
        this.mobile = mobile;
        this.authText = authText;
    }
}
