package cn.kgc.tangcco.pojo;

import cn.hutool.core.util.IdcardUtil;
import cn.kgc.tangcco.utils.localdate.BaseLocalDateUtil;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author 李昊哲
 * @Description 用户信息项目运行初期无需收集用户信息的时候可以暂时无需使用
 * @create 2021/1/23 11:18
 * 用户年龄 ，出生日期 ， 性别 可根据身份证解析也可以通过数据库生成视图
 */
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class User implements Serializable {
    private static final long serialVersionUID = -7677083846075670795L;
    /**
     * 用户主键ID
     */
    private Long uid;
    /**
     * 用户唯一身份标识 32位长度根据当前时间生成的随机字符串
     */
    private String uuid;
    /**
     * 用户昵称 初始化为用户的手机号
     */
    private String nickname;
    /**
     * 用户真实姓名
     */
    private String realname;
    /**
     * 中国身份证
     */
    private String idCard;

    /**
     * 年龄 根据身份证计算 无需输入
     */
    private Integer age;

    /**
     * 出生日期 根据身份证计算 无需输入
     */
    @JSONField(format = "yyyyMMdd")
    private LocalDate dateOfBirth;

    /**
     * 性别 根据身份证计算 无需输入
     * 1代表男性
     * 0代表女性
     */
    private Integer gender;

    /**
     * 账号id与账号绑定
     */
    private Long aid;

    /**
     * 角色id与身份绑定
     */
    private Long rid;

    /**
     * @param nickname
     * @param realname
     * @param idCard
     * @param age
     * @param aid
     */
    public User(String nickname, String realname, String idCard, Integer age, Long aid) {
        this.nickname = nickname;
        this.realname = realname;
        setIdCard(idCard);
        this.age = age;
        this.aid = aid;
    }

    public void setIdCard(String idCard) {
        String pattern = "(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)";
        if (idCard.matches(pattern)) {
            this.idCard = idCard;
            this.age = IdcardUtil.getAgeByIdCard(this.idCard);
            this.dateOfBirth = BaseLocalDateUtil.parse(IdcardUtil.getBirth(this.idCard), "yyyyMMdd");
            this.gender = IdcardUtil.getGenderByIdCard(this.idCard);
        }
    }
}
