package cn.kgc.tangcco.action; /**
 * @author 李昊哲
 * @Description
 * @create 2021/1/25 10:02
 */

import cn.hutool.core.util.IdUtil;
import cn.kgc.tangcco.model.ResponseText;
import cn.kgc.tangcco.model.ResultCode;
import cn.kgc.tangcco.utils.servlet.BaseJsonp;
import cn.kgc.tangcco.utils.servlet.BaseServlet;
import cn.kgc.tangcco.utils.token.BaseJwtUtil;
import com.alibaba.fastjson.JSON;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "SendCode", value = "/sendCode.action")
public class SendCode extends HttpServlet {
    // 验证手机号的正则表达式
    private final static String mobileRex = "^[1][3,4,5,6,7,8,9][0-9]{9}$";
    // jwt密钥
    private final static String key = "lihaozhelihaozhelihaozhelihaozhelihaozhelihaozhe";

    // token有效期
    private static Long expiryDate = 60000L;

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String mobile = request.getParameter("mobile");
        if (StringUtils.isBlank(mobile)) {
            // 手机号不能未空
            ResponseText<String> responseText = new ResponseText<>(ResultCode.MOBILE_NULL.getCode(), ResultCode.MOBILE_NULL.getMsg(), ResultCode.MOBILE_NULL.getDesc());
            BaseServlet.printJsonObject(response, responseText);
            return;
        }
        if (!mobile.matches(mobileRex)) {
            ResponseText<String> responseText = new ResponseText<>(ResultCode.MOBILE_INCORRECT.getCode(), ResultCode.MOBILE_INCORRECT.getMsg(), ResultCode.MOBILE_INCORRECT.getDesc());
            BaseServlet.printJsonObject(response, responseText);
            return;
        }
        String token = request.getHeader("token");
        if (StringUtils.isBlank(token)) {
            // token不存在视为第一次访问
            // 发送短信 并获取验证码
            ResponseText<String> responseText = sendCode(response, mobile, expiryDate);
            BaseServlet.printJsonObject(response, responseText);
            return;
        } else {
            // token存在解析token 如果toekn失效说明 可以再次发送短信
            // token存在解析token
            // 能够正常解析则验证token中的手机号与获取的手机号是否一致
            // 一致则说明一分钟内该手机号二次发送短信不可发送
            // 不一致则说明使用了新的手机号注册跨域发送短信
            ResponseText<DecodedJWT> responseText = BaseJwtUtil.jwtDecode(token, key);
            switch (responseText.getCode()) {
                case "200": {
                    if (responseText.getData().getClaim("mobile").asString().equals(mobile)) {
                        // 能够正常解析则验证token中的手机号与获取的手机号是否一致 一致则说明一分钟内该手机号二次发送短信不可发送
                        ResponseText<String> text = new ResponseText<>(ResultCode.NO_TIMEOUT.getCode(), ResultCode.NO_TIMEOUT.getMsg(), ResultCode.NO_TIMEOUT.getDesc());
                        BaseServlet.printJsonObject(response, text);
                    } else {
                        // 能够正常解析则验证token中的手机号与获取的手机号是否一致 不一致则说明使用了新的手机号注册跨域发送短信
                        ResponseText<String> code = sendCode(response, mobile, expiryDate);
                        BaseServlet.printJsonObject(response, code);
                    }
                    return;
                }
                case "10014": {
                    // token失效
                    ResponseText<String> code = sendCode(response, mobile, expiryDate);
                    BaseServlet.printJsonObject(response, code);
                    return;
                }
                default: {
                    BaseServlet.printJsonObject(response, responseText);
                    return;
                }
            }
        }
    }

    /**
     * @param response   HttpServletResponse
     * @param mobile     手机号
     * @param expiryDate 有效期 单位毫秒
     * @return
     */
    public ResponseText<String> sendCode(HttpServletResponse response, String mobile, Long expiryDate) {
        String code = RandomStringUtils.randomNumeric(4);
        if (code == null) {
            // 验证码发送失败
            return new ResponseText<>(ResultCode.UNKNOWN_HOST.getCode(), ResultCode.UNKNOWN_HOST.getMsg(), null);
        }
        System.out.println(code);
        // 验证码发送成功生成token
        // payload
        Map<String, String> param = new HashMap<>();
        param.put("mobile", mobile);
        param.put("code", code);
        // 生成有效期未1分钟的token
        String token = BaseJwtUtil.jwtEncode(key, param, expiryDate);
        return new ResponseText<>(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMsg(), token);
    }
}
