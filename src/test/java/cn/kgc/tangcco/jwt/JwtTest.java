package cn.kgc.tangcco.jwt;

import cn.kgc.tangcco.model.ResponseText;
import cn.kgc.tangcco.pojo.Account;
import cn.kgc.tangcco.utils.token.BaseJwtUtil;
import com.alibaba.fastjson.JSON;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.AlgorithmMismatchException;
import com.auth0.jwt.exceptions.InvalidClaimException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 李昊哲
 * @Description
 * @create 2020/12/29 21:58
 */
public class JwtTest {
    @Test
    public void generate(){
        Account account = new Account("15311484568", "123456");
        String token = JWT.create()
                .withClaim("mobile","15311484568")
                .withExpiresAt(new Date(System.currentTimeMillis() + 60 * 1000))  //设置过期时间
                .withAudience("authentication") //设置接受方信息，一般时登录用户
                .withIssuedAt(new Date()) // 签发时间
                .sign(Algorithm.HMAC256("lihaozhe"));  //使用HMAC算法，lihaozhe作为密钥加密
        System.out.println(token);
    }

    @Test
    public void parse(){
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJhdXRoZW50aWNhdGlvbiIsIm1vYmlsZSI6IjE1MzExNDg0NTY4IiwiZXhwIjoxNjA5Mjg3MzAxLCJpYXQiOjE2MDkyODcyNDF9.GCSUFwwZoMOALrJ4NzGMBJDP629Y7o45MKRMQhzYwK0";

        // 获取接收方
        // String audience = JWT.decode(token).getAudience().get(0);
        // 验证接受方
        // Assertions.assertEquals("authentication", audience);
        try {
            JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256("lihaozhe")).build();
            DecodedJWT verify = jwtVerifier.verify(token);
            System.out.println(JSON.toJSONString(verify.getClaim("mobile").asString()));
        }catch (SignatureVerificationException e){
            System.out.println("签名不一致");
        }catch (AlgorithmMismatchException e){
            System.out.println("算法不匹配");
        }catch (TokenExpiredException e){
            System.out.println("token过期失效");
        }catch (InvalidClaimException e){
            System.out.println("payload失效");
        }
    }
    @Test
    public void encode(){
        Map<String, String> map = new HashMap<>();
        Account account = new Account("15311484568", "123456");
        map.put("account", JSON.toJSONString(account));
        String token = BaseJwtUtil.jwtEncode("lihaozhe", map, 60 * 1000);
        System.out.println(token);
        ResponseText<DecodedJWT> responseText = BaseJwtUtil.jwtDecode(token, "lihaozhe");
        String text = responseText.getData().getClaim("account").asString();
        System.out.println(JSON.toJSONString(text));
    }
}
